#!/bin/bash

docker-compose pull
docker rmi $(docker images | grep "^<none>" | awk "{print $3}")
docker-compose -f docker-compose.yml -f docker-compose.prod.secret.yml up -d
