defmodule PhoneMonitoring.Repo.Migrations.CreateReporter do
  use Ecto.Migration

  def change do
    create table(:reporters) do
      add :name, :string
      add :username, :string, null: false
      add :password_hash, :string

    end

    create unique_index(:reporters, [:username])
  end
end
