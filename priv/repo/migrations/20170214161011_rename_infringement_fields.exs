defmodule PhoneMonitoring.Repo.Migrations.RenameInfringementFields do
  use Ecto.Migration

  def change do
    alter table(:infringements) do
      remove :student
      add :student_id, references(:students, on_delete: :nothing)

      remove :reporter
      add :reporter_id, references(:reporters, on_delete: :nothing)
    end
  end
end
