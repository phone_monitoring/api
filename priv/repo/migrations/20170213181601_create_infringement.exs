defmodule PhoneMonitoring.Repo.Migrations.CreateInfringement do
  use Ecto.Migration

  def change do
    create table(:infringements) do
      add :student, references(:students, on_delete: :nothing)
      add :reporter, references(:reporters, on_delete: :nothing)

      timestamps()
    end
    create index(:infringements, [:student])
    create index(:infringements, [:reporter])

  end
end
