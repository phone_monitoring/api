defmodule PhoneMonitoring.Repo.Migrations.ClassTeacherEmail do
  use Ecto.Migration

  def change do
    create table(:class_teachers) do
      add :class, :string, null: false
      add :email, :string, null: false
    end

    create unique_index(:class_teachers, [:class])
  end
end
