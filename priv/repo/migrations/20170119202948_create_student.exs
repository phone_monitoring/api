defmodule PhoneMonitoring.Repo.Migrations.CreateStudent do
  use Ecto.Migration

  def change do
    create table(:students) do
      add :name, :string
      add :email, :string
      add :class, :string
    end
  end
end
