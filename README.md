# PhoneMonitoring API

[![build status](https://gitlab.com/phone_monitoring/api/badges/master/build.svg)](https://gitlab.com/phone_monitoring/api/commits/master)
[![coverage report](https://gitlab.com/phone_monitoring/api/badges/master/coverage.svg)](https://gitlab.com/phone_monitoring/api/commits/master)


PhoneMonitoring ist ein WebService, mit dem die Handynutzung von Schülern gemeldet werden kann.

Bei Verstößen werden automatisch die Eltern via Email benachrichtigt.

Dieses Projekt beheimatet die API.

## Tech Stack
Die API ist in Elixir mit dem Phoenix Web Framework geschrieben. Als Datenbank wird PostgresQL verwendet. Deploys erfolgen automatisch nach CI Tests auf einen Heroku-Server.

## Lokal laufen lassen
Ihr müsst Elixir installiert haben.

```
$ mix deps.get
$ mix phoenix.server
```

## Konsequenzen
Die Regeln nach denen das System modeliert ist finden sich [hier](https://sv.cjd-rostock.de/infos/dokumente/allgemein/Handyordnung.pdf).

## Version numbers
To ensure a working bulid, set the version number to the correct value in the following files:
- `Dockerfile`
- `mix.exs`

## Running on your server
Your server must have Docker and Docker Compose installed.
Simply clone this repo onto your server.
You need to create a file called `docker-compose.prod.secret.yml`. This is where your production config will happen.

It will look something like this:
```yml
web:
  environment:
    SMTP_USERNAME: example
    SMTP_USERNAME: example
    SMTP_SERVER: example.com
    SMTP_PORT: 25
    SECRET_KEY_BASE: somesupercomplicatedstring
    POOL_SIZE: 18
    
```

Then you may simply run `script/start-with-secrets`. Note that it may require
sudo.

The script will get all necesseary docker images and start all containers. To
update to a newer version or reload the config file, simply run the script
again.

Via default the API Server will be avalible on port 5001. Set the PORT property
in the `web` section of the config to overwrite this.
