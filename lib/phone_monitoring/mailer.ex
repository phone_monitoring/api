defmodule PhoneMonitoring.Mailer do
  use Bamboo.Mailer, otp_app: :phone_monitoring

  def deliver_all(emails) do
    Enum.each(emails, &deliver_now/1)
  end
end
