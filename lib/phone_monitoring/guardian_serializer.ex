defmodule PhoneMonitoring.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias PhoneMonitoring.Repo
  alias PhoneMonitoring.Reporter

  def for_token(reporter = %Reporter{}), do: {:ok, "Reporter:#{reporter.id}"}
  def for_token(_), do: {:error, "Unknown resource type"}

  def from_token("Reporter:" <> id), do: {:ok, Repo.get(Reporter, id)}
  def from_token(_), do: {:error, "Unknown resource type"}
end
