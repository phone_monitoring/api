defmodule PhoneMonitoring.InfringementEmail do
  use Bamboo.Phoenix, view: PhoneMonitoring.EmailView

  @sender_email "handy.rostock@cjd-nord.de"
  @error_email "benjustusbals+phonemonitoring_error@gmail.com"

  def notify(student, times, reporter) do
    class_teacher = PhoneMonitoring.Repo.get_by(PhoneMonitoring.ClassTeacher, class: student.class)

    to_parents = notify_parents(student, times, class_teacher)
    to_class_teacher = notify_class_teacher(student, times, class_teacher, reporter)

    List.flatten([to_class_teacher, to_parents])
  end

  def notify_parents(student, times, class_teacher) do
    new_email
    |> to(student.email)
    |> from(@sender_email)
    |> put_header("Reply-To", class_teacher_email_info(class_teacher, student))
    |> select_email_parents(times, %{student_name: student.name, times: times, student_class: student.class})

  end

  def notify_class_teacher(student, times, %{email: email} = class_teacher, reporter) do
    new_email
    |> to(class_teacher_email_info(class_teacher, student))
    |> from(@sender_email)
    |> put_header("Reply-To", student.email)
    |> select_email_class_teacher(times, %{student_name: student.name, reporter_name: reporter.name, times: times, student_class: student.class})
  end

  def notify_class_teacher(student, times, _, reporter) do
    error_email = error_email(student)
    [error_email, notify_class_teacher(student, times, %{email: @error_email}, reporter)]
  end

  def error_email(student) do
    error_email = new_email
    |> to(@error_email)
    |> from(@sender_email)
    |> render_error_email(%{student_name: student.name, student_class: student.class})
  end

  def class_teacher_email_info(%{email: email}, student) do
    email
  end

  def class_teacher_email_info(_, _) do
    @error_email
  end

  def select_email_parents(email, times, assigns) do
    case times do
      1 -> first_email_parents(email, assigns)
      2 -> second_email_parents(email, assigns)
      _ -> nth_email_parents(email, assigns)
    end
  end

  def select_email_class_teacher(email, times, assigns) do
    case times do
      1 -> first_email_class_teacher(email, assigns)
      2 -> second_email_class_teacher(email, assigns)
      _ -> nth_email_class_teacher(email, assigns)
    end
  end

  def first_email_parents(email, assigns) do
    email
    |> subject("Verstoß gegen die Handy-Ordnung")
    |> render("first_email_parents.html", assigns)
  end

  def second_email_parents(email, assigns) do
    email
    |> subject("Erneuter Verstoß gegen die Handy-Ordnung")
    |> render("second_email_parents.html", assigns)
  end

  def nth_email_parents(email, assigns) do
    email
    |> subject("#{assigns.times}. Verstoß gegen die Handy-Ordnung")
    |> render("nth_email_parents.html", assigns)
  end

  def first_email_class_teacher(email, assigns) do
    email
    |> subject("Verstoß gegen die Handy-Ordnung")
    |> render("first_email_class_teacher.html", assigns)
  end

  def second_email_class_teacher(email, assigns) do
    email
    |> subject("Erneuter Verstoß gegen die Handy-Ordnung")
    |> render("second_email_class_teacher.html", assigns)
  end

  def nth_email_class_teacher(email, assigns) do
    email
    |> subject("#{assigns.student_name}: #{assigns.times}. Verstoß gegen die Handy-Ordnung")
    |> render("nth_email_class_teacher.html", assigns)
  end

  def render_error_email(email, assigns) do
    email
    |> subject("Fehler beim Zustellen einer E-Mail an einen Klassenleiter")
    |> render("error_email.html", assigns)
  end

  def error_email_address(), do: @error_email
end
