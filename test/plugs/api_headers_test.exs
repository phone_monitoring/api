defmodule PhoneMonitoring.ApiHeadersTest do
  use ExUnit.Case, async: true
  use Plug.Test

  alias PhoneMonitoring.ApiHeaders

  test "init does nothing" do
    assert ApiHeaders.init([]) == []
    assert ApiHeaders.init(ben: true) == [ben: true]
  end

  test "call puts the access-control-allow-origin header" do
    conn = ApiHeaders.call(conn(:get, "/"), [])
    assert get_resp_header(conn, "access-control-allow-origin") == ["*"]
  end

  test "call puts the access-control-allow-headers header" do
    conn = ApiHeaders.call(conn(:get, "/"), [])
    assert get_resp_header(conn, "access-control-allow-headers") == ["Authorization, Content-Type"]
  end
end
