defmodule PhoneMonitoring.InfringementTest do
  use PhoneMonitoring.ModelCase

  alias PhoneMonitoring.{Infringement, Reporter, Student}

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    reporter = %Reporter{id: 1, username: "ben", name: "Ben Bals", password_hash: "whatever"}
    student = %Student{id: 1, name: "Bob", email: "aeu", class: "11b"}
    changeset = Infringement.changeset(%Infringement{}, %{student: student, reporter: reporter})
    assert changeset.valid?
  end

  test "incomplete changeset" do
    changeset = Infringement.changeset(%Infringement{}, %{})
    refute changeset.valid?
  end
end
