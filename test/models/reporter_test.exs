defmodule PhoneMonitoring.ReporterTest do
  use PhoneMonitoring.ModelCase

  alias PhoneMonitoring.Reporter

  test "registration changset with valid params" do
    changeset = Reporter.registration_changeset(%Reporter{}, %{name: "Ben Bals", username: "ben", password: "12345678"})

    assert changeset.valid?
  end

  test "registration changset with too short password" do
    changeset = Reporter.registration_changeset(%Reporter{}, %{username: "ben", name: "Ben Bals", password: "1234"})

    assert {:password, {"should be at least %{count} character(s)", [count: 8, validation: :length, min: 8]}} in changeset.errors
  end

  test "name is required" do
    assert {:name, "can't be blank"} in errors_on(%Reporter{}, %{username: "ben"})
  end

  test "username is required" do
    assert {:username, "can't be blank"} in errors_on(%Reporter{}, %{name: "Ben Bals"})
  end

  test "username can't be shorter than 2 characters" do
    assert {:username, "should be at least 2 character(s)"} in errors_on(%Reporter{}, %{username: "b", name: "Ben Bals"})
  end

  test "changeset downcases usernames" do
    changeset = Reporter.changeset(%Reporter{}, %{username: "Ben"})

    assert Map.get(changeset.changes, :username) == "ben"
  end
end
