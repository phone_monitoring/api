defmodule PhoneMonitoring.ClassControllerTest do
  use PhoneMonitoring.ConnCase, async: false
  alias PhoneMonitoring.ClassController

  setup do
    insert_students_and_class_teachers()
    reporter = insert_reporter()
    {:ok, jwt, _full_claims} = Guardian.encode_and_sign(reporter)
    {:ok, %{jwt: jwt}}
  end

  test "GET /api/classes returs a list of classes", %{conn: conn, jwt: jwt} do
    conn =
      conn
      |> authorize(jwt)
      |> get("/api/classes")

    assert conn.resp_body == "{\"data\":[\"6b\",\"11b\",\"11c\"]}"
  end

  test "GET /api/classes is protected by auth", %{conn: conn} do
    conn = get(conn, "/api/classes")

    assert conn.status == 401
  end

  test "sorting works right" do
    unsorted = ["11b", "5b", "12a1", "6as", "12e", "11a1", "10b"]
    solution = ["5b", "6as", "10b", "11a1", "11b", "12a1", "12e"]
    sorted = ClassController.sort_classes(unsorted)

    assert sorted == solution
  end

end
