defmodule PhoneMonitoring.StudentControllerTest do
    use PhoneMonitoring.ConnCase, async: false

    setup do
      insert_students_and_class_teachers()
      reporter = insert_reporter()
      {:ok, jwt, _full_claims} = Guardian.encode_and_sign(reporter)
      {:ok, %{jwt: jwt}}
    end

    test "GET student controller refuses unauthorized request", %{conn: conn} do
      conn = get conn, "/api/students"

      assert conn.status == 401
      assert conn.resp_body =~ "invalid token"
    end

    test "GET /api/students returns a list of students", %{conn: conn, jwt: jwt} do
      conn =
        conn
        |> authorize(jwt)
        |> get("/api/students")

      returned_students = parse_students_json_from_conn(conn)

      assert length(returned_students) == length(test_student_data())
      assert List.first(returned_students).name ==  "Ben Bals"
      assert conn.status == 200
    end

    test "GET /api/students?class=6b only returns students of that class (in right order)", %{conn: conn, jwt: jwt} do
      conn =
        conn
        |> authorize(jwt)
        |> get("/api/students?class=6b")

      returned_students = parse_students_json_from_conn(conn)

      assert conn.status == 200
      assert List.first(returned_students).name == "Oscar Wilde"
      Enum.each(returned_students, fn student ->
        assert student.class == "6b"
      end)
    end

    test "GET /api/students?class=notknown returns an error 404", %{conn: conn, jwt: jwt} do
      conn =
        conn
        |> authorize(jwt)
        |> get("/api/students?class=notknown")

      assert conn.status == 404
      assert conn.resp_body =~ "invalid class"
    end
end
