defmodule PhoneMonitoring.ApiControllerTest do
  use PhoneMonitoring.ConnCase, async: true

  test "OPTIONS /api/anything gives the right access-control-allow-methods", %{conn: conn} do
    conn = options(conn, "/api/anything")

    assert get_resp_header(conn, "access-control-allow-methods") == ["POST, GET, OPTIONS"]
  end
end
