defmodule PhoneMonitoring.ReporterControllerTest do
  use PhoneMonitoring.ConnCase, async: false
  alias PhoneMonitoring.Reporter

  test "POST /reporters succesfully", %{conn: conn} do
    body = """
      {
    "admin_password": "TEST",
    "data":
        [{"username": "tester", "password": "12345678", "name": "chester tester"}]
      }
    """

    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/reporters", body)

    assert conn.resp_body =~ "reporters created successfully"
    assert conn.status == 201

    reporter = Repo.get_by!(Reporter, username: "tester")

    assert reporter.name === "chester tester"
    assert {:ok, _} = PhoneMonitoring.LoginController.find_and_confirm_password(%{"username" => "tester", "password" => "12345678"})
  end
end
