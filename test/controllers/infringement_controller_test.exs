defmodule PhoneMonitoring.InfringementControllerTest do
  use PhoneMonitoring.ConnCase, async: true
  use Bamboo.Test

  alias PhoneMonitoring.{Infringement, InfringementEmail, Student, Reporter}

  setup do
    reporter = insert_reporter()
    insert_students_and_class_teachers()
    {:ok, jwt, _full_claims} = Guardian.encode_and_sign(reporter)
    {:ok, %{jwt: jwt, reporter: reporter}}
  end

  def assert_emails_sent(emails) do
    Enum.each(emails, fn email -> assert_delivered_email(email) end)
  end

  test "POST /infringements with valid id works and sends first email", %{jwt: jwt, conn: conn, reporter: reporter} do

    student = Repo.get_by!(Student, name: "Ben Bals")

    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": #{student.id}}")


    assert_emails_sent(InfringementEmail.notify(student, 1, reporter))
    assert conn.status == 201
    assert conn.resp_body =~ "infringement reported successfully"
    assert conn.resp_body =~ "success"
  end

  test "POST /infringements with invalid id", %{conn: conn, jwt: jwt} do
    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": 123}")

    assert conn.status == 422
    assert conn.resp_body =~ "infringement could not be reported"
    assert conn.resp_body =~ "error"
  end

  test "POST /infringements with no id gives an error", %{conn: conn, jwt: jwt} do
    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{}")

    assert conn.status == 422
    assert conn.resp_body =~ "infringement could not be reported"
    assert conn.resp_body =~ "error"
  end

  test "POST /infringements unauthorized", %{conn: conn} do
    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": 1}")

    assert conn.status == 401
    assert conn.resp_body =~ "invalid token"
  end

  test "deleted reporters can't report anymore", %{conn: conn, jwt: jwt, reporter: reporter} do
    student_id = Repo.get_by!(Student, name: "Ben Bals").id
    Repo.delete(reporter)

    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": #{student_id}}")

    assert conn.status == 401
    assert conn.resp_body =~ "invalid token"

  end

  test "second infringement sends right email", %{conn: conn, jwt: jwt} do
    student = Repo.get_by!(Student, name: "Ben Bals")
    reporter = Repo.get_by!(Reporter, username: "test")

    # insert first infringement, so the request will be the second one
    changeset = Infringement.changeset(%Infringement{}, %{reporter: reporter, student: student})
    Repo.insert!(changeset)

    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": #{student.id}}")

    assert_emails_sent(InfringementEmail.notify(student, 2, reporter))
  end

  test "nth ()n > 2) infringement sends right email", %{conn: conn, jwt: jwt} do
    student = Repo.get_by!(Student, name: "Ben Bals")
    reporter = Repo.get_by!(Reporter, username: "test")

    for n <- 1..2 do
      changeset = Infringement.changeset(%Infringement{}, %{reporter: reporter, student: student})
      Repo.insert!(changeset)
    end

    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": #{student.id}}")

    assert_emails_sent(InfringementEmail.notify(student, 3, reporter))
  end

  test "emails get sent even if no class teacher is found", %{conn: conn, jwt: jwt} do
    student = Repo.get_by!(Student, name: "Sebastian Schröder")
    reporter = Repo.get_by!(Reporter, username: "test")


    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": #{student.id}}")

    error_address = %{email: InfringementEmail.error_email_address()}

    assert_delivered_email(InfringementEmail.error_email(student))
    assert_delivered_email(InfringementEmail.notify_class_teacher(student, 1, error_address, reporter))
    assert_delivered_email(InfringementEmail.notify_parents(student, 1, nil))
  end

  test "reporter can't report more then 20 infringements per day", %{conn: conn, jwt: jwt} do
    student = Repo.get_by!(Student, name: "Ben Bals")
    reporter = Repo.get_by!(Reporter, username: "test")

    for n <- 1..20 do
      changeset = Infringement.changeset(%Infringement{}, %{reporter: reporter, student: student})
      Repo.insert!(changeset)
    end

    conn =
      conn
      |> authorize(jwt)
      |> put_req_header("content-type", "application/json")
      |> post("/api/infringements", "{\"id\": #{student.id}}")

    assert conn.resp_body =~ "maximum number of infringements in 24 hours exceeded"
    assert conn.status == 423

  end

end
