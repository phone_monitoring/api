defmodule PhoneMonitoring.LoginControllerTest do
  use PhoneMonitoring.ConnCase, async: true

  setup do
    reporter = insert_reporter()
    {:ok, jwt, _full_claims} = Guardian.encode_and_sign(reporter)
    {:ok, %{jwt: jwt}}
  end

  test "logging in with valid credentials works", %{conn: conn} do
    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/login", "{\"username\":\"test\", \"password\":\"testtest\"}")

    returned_json = Poison.Parser.parse!(conn.resp_body)

    assert conn.status == 200
    assert Map.fetch!(returned_json, "token")
  end


  test "logging in with invalid password doesnt work", %{conn: conn} do
    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/login", "{\"username\":\"test\", \"password\":\"testtest12\"}")

    returned_json = Poison.Parser.parse!(conn.resp_body)

    assert conn.status == 401
    refute Map.get(returned_json, "token")
    assert returned_json == %{"error" => "invalid username or password"}

  end

  test "logging in with invalid username doesnt work", %{conn: conn} do
    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/login", "{\"username\":\"test123\", \"password\":\"testtest\"}")

    returned_json = Poison.Parser.parse!(conn.resp_body)

    assert conn.status == 401
    refute Map.get(returned_json, "token")
    assert returned_json == %{"error" => "invalid username or password"}
  end

  test "login username is downcased and trimmed", %{conn: conn} do
    conn =
      conn
      |> put_req_header("content-type", "application/json")
      |> post("/api/login", "{\"username\":\"  Test  \", \"password\":\"testtest\"}")

    assert conn.status == 200
  end
end
