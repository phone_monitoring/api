defmodule PhoneMonitoring.TestHelpers do
  alias PhoneMonitoring.{Repo, Student, Reporter, ClassTeacher}

  def authorize(conn, jwt) do
    conn
    |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
  end


  def insert_students_and_class_teachers() do
    test_student_data
    |> Enum.map(fn student -> Student.changeset(%Student{}, student) end)
    |> Enum.map(fn student -> Repo.insert!(student, []) end)

    test_class_teacher_data
    |> Enum.map(fn class_teacher -> ClassTeacher.changeset(%ClassTeacher{}, class_teacher) end)
    |> Enum.map(fn class_teacher -> Repo.insert!(class_teacher, []) end)
  end

  def insert_reporter() do
    Reporter.registration_changeset(%Reporter{}, test_reporter)
    |> Repo.insert!()
  end

  def test_student_data do
    [
      %{name: "Wilem Schmitter", email: "wilems.eltern@schmitter.de", class: "6b"},
      %{name: "Oscar Wilde", email: "wilems.eltern@schmitter.de", class: "6b"},
      %{name: "Ben Bals", email: "b.bals@outlook.com", class: "11b"},
      %{name: "Erik Jenß", email: "vater.jenss@online.de", class: "11b"},
      %{name: "Sebastian Schröder", email: "basti@gmx.de", class: "11c"}
    ]
  end

  def test_class_teacher_data do
    [
      %{class: "11b", email: "11b@cjd-nord.de"},
      %{class: "6b", email: "6b@cjd-nord.de"}
    ]
  end

  def test_reporter do
    %{
      name: "Tester",
      username: "test",
      password: "testtest"
    }
  end

  def parse_students_json_from_conn(conn) do
    conn.resp_body
    |> Poison.decode!(as: %{"data" => [%PhoneMonitoring.Student{}]})
    |> Map.fetch!("data")
  end
end
