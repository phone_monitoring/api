defmodule PhoneMonitoring.ClassTeacher do
  use PhoneMonitoring.Web, :model

  schema "class_teachers" do
    field :class, :string
    field :email, :string
  end

  def changeset(class_teacher, params \\ %{}) do
    class_teacher
    |> cast(params, [:class, :email])
    |> validate_required([:class, :email])
  end
end
