defmodule PhoneMonitoring.Reporter do
  use PhoneMonitoring.Web, :model

  schema "reporters" do
    field :username, :string
    field :name, :string # to be used in the E-Mail
    field :password_hash, :string
    field :password, :string, virtual: true
  end

  def changeset(model, params \\ %{}) do
    params = Map.update(params, :username, nil, &String.downcase/1)
    model
    |> cast(params, [:username, :name])
    |> validate_required([:username, :name])
    |> validate_length(:username, min: 2)
    |> unique_constraint(:username)
  end

 
  def registration_changeset(model, params \\ %{}) do
    model
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_length(:password, min: 8, max: 200)
    |> put_password_hash()
  end

  def put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))
      _ ->
        changeset
    end
  end
end
