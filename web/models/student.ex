defmodule PhoneMonitoring.Student do
  use PhoneMonitoring.Web, :model

  schema "students" do
    field :name, :string
    field :email, :string
    field :class, :string
  end

  def changeset(student, params \\ %{}) do
    student
    |> cast(params, [:name, :email, :class])
    |> validate_required([:name, :email, :class])
  end
end