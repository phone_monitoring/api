defmodule PhoneMonitoring.Infringement do
  use PhoneMonitoring.Web, :model

  schema "infringements" do
    belongs_to :student, PhoneMonitoring.Student
    belongs_to :reporter, PhoneMonitoring.Reporter

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> safe_put_assoc(:student, params)
    |> assoc_constraint(:student)
    |> foreign_key_constraint(:student_id)
    |> safe_put_assoc(:reporter, params)
    |> assoc_constraint(:reporter)
    |> validate_required([])
  end

  def safe_put_assoc(changeset, title, params) do
    case Map.fetch(params, title) do
      :error -> add_error(changeset, title, "invalid association")
      {:ok, value} -> put_assoc(changeset, title, value)
    end
  end
end
