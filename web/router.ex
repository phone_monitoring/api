defmodule PhoneMonitoring.Router do
  use PhoneMonitoring.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug PhoneMonitoring.ApiHeaders
  end

  pipeline :api_auth do
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
  end

  scope "/api/", PhoneMonitoring do
    pipe_through [:api, :api_auth]

    options "/:anything", ApiController, :options

    post "/login", LoginController, :login

    get "/students", StudentController, :index
    get "/classes", ClassController, :index

    post "/infringements", InfringementController, :create
    post "/reporters", ReporterController, :create

  end

  if Mix.env == :dev do
    scope "/dev" do
      # pipe_through [:browser]

      forward "/sent_emails", Bamboo.EmailPreviewPlug
    end
  end

end
