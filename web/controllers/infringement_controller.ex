defmodule PhoneMonitoring.InfringementController do
  use PhoneMonitoring.Web, :controller
  alias PhoneMonitoring.{Infringement, Student, InfringementEmail, Mailer}
  require Logger

  plug Guardian.Plug.EnsureAuthenticated, handler: PhoneMonitoring.LoginController
  plug Guardian.Plug.EnsureResource, handler: PhoneMonitoring.LoginController

  @max_number_of_infringement_per_day_per_reporter 20
  @default_error "infringement could not be reported"

  def create(conn, %{"id" => id}) when is_integer(id) do
    reporter = Guardian.Plug.current_resource(conn)
    student = Repo.get(Student, id)
    changeset = Infringement.changeset(%Infringement{}, %{student: student, reporter: reporter})

    cond do
      is_nil(reporter) or is_nil(student) -> error(conn)
      number_of_reports_current_day(reporter) >= @max_number_of_infringement_per_day_per_reporter ->
        Logger.warn("#{reporter.name} tried to report #{student.name}, but has exceeded his max number of infringements")
        error(conn,
          %{
            message: "maximum number of infringements in 24 hours exceeded",
            status: :locked
          }
        )
      true ->
        conn = insert_infringement(conn, changeset)
        number_of_infringements = Repo.one(number_of_infringements_query(student))
        Logger.info("#{reporter.name} reported #{student.name}, #{number_of_infringements}th infringement")
        InfringementEmail.notify(student, number_of_infringements, reporter) |> Mailer.deliver_all()
        conn
    end
  end

  def create(conn, _), do: error(conn)

  def insert_infringement(conn, changeset) do
    case Repo.insert(changeset) do
      {:ok, _infringement} ->
        conn
        |> put_status(:created)
        |> render("success.json")
      {:error, _changeset} -> error(conn)
    end
  end

  defp error(conn, %{message: message, status: status}) do
    message = if message, do: message, else: "infringement could not be reported"
    status = if status, do: status, else: :unprocessable_entity
    Logger.error("Unsuccessfull reporting, reason: #{message}")
    conn
    |> put_status(status)
    |> render(PhoneMonitoring.ErrorView, "error.json", %{message: message})
  end

  defp error(conn) do
    error(conn, %{message: nil, status: nil})
  end

  defp number_of_infringements_query(student) do
    from i in Infringement,
      where: i.student_id == ^student.id,
      select: count("*")
  end

  def number_of_reports_current_day(reporter) do
    query =
      from i in Infringement,
      select: count(i.id),
      where: i.reporter_id == ^reporter.id and i.inserted_at > ago(24, "hour")

    Repo.one(query)
  end
end
