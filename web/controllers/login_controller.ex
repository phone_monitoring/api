defmodule PhoneMonitoring.LoginController do
  use PhoneMonitoring.Web, :controller
  require Logger

  alias PhoneMonitoring.Reporter

  def login(conn, params) do
    case find_and_confirm_password(conn.body_params) do
      {:ok, reporter} ->
        new_conn = Guardian.Plug.api_sign_in(conn, reporter)
        jwt = Guardian.Plug.current_token(new_conn)
        {:ok, claims} = Guardian.Plug.claims(new_conn)
        exp = Map.get(claims, "exp")

        Logger.info("#{reporter.name} logged in")

        new_conn
        |> put_resp_header("authorization", "Bearer #{jwt}")
        |> put_resp_header("x-expires", to_string(exp))
        |> render("login.json", reporter: reporter, jwt: jwt, exp: exp)
      :error ->
        username = Map.get(conn.body_params, "username", "no username provided")
        Logger.warn("+++++ Unsuccessfull login try to account #{username} +++++")
        Logger.warn(inspect(conn.body_params))
        Logger.warn("+++++ Unsuccessfull login end ++++++++++++++++++++++++++++")
        conn
        |> put_status(401)
        |> render(PhoneMonitoring.ErrorView, "error.json", message: "invalid username or password")
    end
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(401)
    |> render(PhoneMonitoring.ErrorView, "error.json", message: "invalid token")
  end

  def no_resource(conn, params), do: unauthenticated(conn, params)

  def find_and_confirm_password(%{"username" => username, "password" => password}) do
    cleaned_username = username |> safe_downcase() |> safe_trim()
    reporter  = Repo.get_by(Reporter, username: cleaned_username)

    cond do
      reporter && Comeonin.Bcrypt.checkpw(password, reporter.password_hash) ->
        {:ok, reporter}
      reporter ->
        :error
      true ->
        Comeonin.Bcrypt.dummy_checkpw()
        :error
    end
  end

  def find_and_confirm_password(_params), do: :error

  def safe_downcase(string) when is_binary(string), do: String.downcase(string)
  def safe_downcase(_), do: nil

  def safe_trim(string) when is_binary(string), do: String.trim(string)
  def safe_trim(_), do: nil
end
