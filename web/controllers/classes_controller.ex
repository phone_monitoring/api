defmodule PhoneMonitoring.ClassController do
  use PhoneMonitoring.Web, :controller
  alias PhoneMonitoring.Student

  plug Guardian.Plug.EnsureAuthenticated, handler: PhoneMonitoring.LoginController

  def index(conn, _params) do
    query = from s in Student,
      distinct: s.class,
      select: s.class

    classes = Repo.all(query) |> sort_classes()

    render conn, classes: classes
  end

  def sort_classes(classes) do
    sorted = Enum.sort(classes, fn a, b ->
      IO.puts(a)
      IO.puts(b)
      %{"letters" => letters_a, "number" => number_string_a} = split_string(a)
      %{"letters" => letters_b, "number" => number_string_b} = split_string(b)


      number_a = String.to_integer(number_string_a)
      number_b = String.to_integer(number_string_b)

      if (number_a != number_b) do
        number_a < number_b
      else
        letters_a < letters_b
      end

    end)
  end

  defp split_string(class_string) do
    regex = ~r/(?<number>\d+)(?<letters>\w+)/
    captures = Regex.named_captures(regex, class_string)
  end
end
