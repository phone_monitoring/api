defmodule PhoneMonitoring.StudentController do
  use PhoneMonitoring.Web, :controller
  alias PhoneMonitoring.Student

  plug Guardian.Plug.EnsureAuthenticated, handler: PhoneMonitoring.LoginController

  def index(conn, %{"class" => class}) do
    students =
      students_by_class_query(class)
      |> Repo.all()
      |> students_to_map
    case students do
      [] -> error_invalid_class(conn)
      _ -> render conn, students: students
    end
  end

  def index(conn, _params) do
    students =
      all_students_query
      |> Repo.all()
      |> students_to_map

    render conn, students: students
  end

  defp error_invalid_class(conn) do
    conn
    |> put_status(404)
    |> render(error: 404)
  end

  defp all_students_query do
    from s in Student,
    select: {s.id, s.name, s.class},
    order_by: s.name
  end

  defp students_by_class_query(class) do
    from s in Student,
    where: s.class == ^class,
    order_by: s.name,
    select: {s.id, s.name, s.class}
  end

  defp students_to_map(students) do
    students
    |> Enum.map(&student_tuple_to_keyword_list/1)
  end

  defp student_tuple_to_keyword_list({id, name, class}) do
    %{
      "id": id,
      "name": name,
      "class": class
    }
  end
end
