defmodule PhoneMonitoring.ReporterController do
  use PhoneMonitoring.Web, :controller
  alias PhoneMonitoring.{Reporter, Repo}

  def create(conn, %{"data" => data, "admin_password" => pass}) do
    if (admin_password() != "" && admin_password == pass) do
      result = do_create(data)
      case result do
        {:ok, message} ->
          conn
          |> put_status(:created)
          |> render("success.json", message: message)
        {:error, message} ->
          conn
          |> put_status(:unprocessable_entity)
          |> render(PhoneMonitoring.ErrorView, "error.json", message: message)
      end
    else
      conn
      |> put_status(:unauthorized)
      |> render(PhoneMonitoring.ErrorView, "error.json", message: "invalid admin password")
    end
  end

  defp admin_password do
    {mix_exists, _} = Code.ensure_loaded(Mix)
    if (mix_exists == :error) do
      System.get_env("ADMIN_PASSWORD")
    else 
      if Mix.env === :test do
        "TEST"
      else
        System.get_env("ADMIN_PASSWORD")
      end
    end
  end

  defp do_create(data) do
    changesets = Enum.map(data, &create_changeset/1)
    all_valid = Enum.all?(changesets, fn x -> x.valid? end)

    if all_valid do

      result = Repo.transaction(fn -> insert_reporters(changesets) end)
      case result do
        {:ok, _} -> {:ok, "reporters created successfully"}
        {:error, _} -> {:error, "invalid reporter data"}
      end

    else
      {:error, "invalid reporter data"}
    end
  end

  defp insert_reporters(changesets) do
    Enum.each(changesets, &Repo.insert!/1)
  end

  defp create_changeset(%{"username" => username, "password" => password, "name" => name}) do
    Reporter.registration_changeset(%Reporter{}, %{
      username: username,
      password: password,
      name: name
    })
  end
end
