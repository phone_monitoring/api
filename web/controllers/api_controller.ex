defmodule PhoneMonitoring.ApiController do
  use PhoneMonitoring.Web, :controller

  def options(conn, _params) do
    conn
    |> put_resp_header("access-control-allow-methods", "POST, GET, OPTIONS")
    |> resp(200, "")
    |> send_resp()
  end
end
