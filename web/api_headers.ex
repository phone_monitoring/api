defmodule PhoneMonitoring.ApiHeaders do
  import Plug.Conn

  def init(options) do
    options
  end

  def call(conn, _options) do
    conn
    |> put_resp_header("access-control-allow-origin", "*")
    |> put_resp_header("access-control-allow-headers", "Authorization, Content-Type")
  end
end
