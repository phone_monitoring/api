defmodule PhoneMonitoring.ClassView do
  def render("index.json", %{classes: classes}) do
    %{
      "data" => classes
    }
  end
end
