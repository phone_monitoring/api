defmodule PhoneMonitoring.StudentView do
  @moduledoc false

  use PhoneMonitoring.Web, :view

  def render("index.json", %{students: students}) do
    %{
      "data" => students
    }
  end

  def render("index.json", %{error: 404}) do
    %{
      "error" => "invalid class"
    }
  end
end
