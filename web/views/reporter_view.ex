defmodule PhoneMonitoring.ReporterView do
  use PhoneMonitoring.Web, :view

  def render("success.json", %{message: message}) do
    %{
      success: message
    }
  end
end
