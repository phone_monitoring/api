defmodule PhoneMonitoring.LoginView do

  use PhoneMonitoring.Web, :view

  def render("login.json", params) do
    %{
      token: params.jwt,
      expires: params.exp
    }
  end
end
