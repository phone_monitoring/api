defmodule PhoneMonitoring.InfringementView do
  use PhoneMonitoring.Web, :view

  def render("success.json", _prams) do
    %{
      success: "infringement reported successfully"
    }
  end
end
