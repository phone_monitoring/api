FROM ubuntu
RUN apt-get update && \
    apt-get install -y libssl1.0.0 postgresql-client locales && \
    apt-get autoclean

RUN mkdir -p /app
ARG VERSION=0.0.2
COPY _build/prod/rel/phone_monitoring/releases/${VERSION}/phone_monitoring.tar.gz /app/phone_monitoring.tar.gz
COPY scripts/wait-for-postgres.sh /app/wait-for-postgres.sh
COPY scripts/on-postgres-up.sh /app/on-postgres-up.sh
WORKDIR /app
RUN tar xvfz phone_monitoring.tar.gz
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PORT 8888
CMD ["/app/bin/phone_monitoring", "foreground"]