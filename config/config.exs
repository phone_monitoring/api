# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :phone_monitoring,
  ecto_repos: [PhoneMonitoring.Repo]

# Configures the endpoint
config :phone_monitoring, PhoneMonitoring.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wihQFNLWoGwqISTi68gbyTRDDkUXBYssJkU4lJ8MOyteg2S93kx/2fCV7L8uuC8v",
  render_errors: [view: PhoneMonitoring.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PhoneMonitoring.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :guardian, Guardian,
  issuer: "PhoneMonitoring",
  ttl: {30, :days},
  allowed_drift: 2000,
  secret_key: "this is a test key",
  serializer: PhoneMonitoring.GuardianSerializer

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
