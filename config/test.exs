use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :phone_monitoring, PhoneMonitoring.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :phone_monitoring, PhoneMonitoring.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "phone_monitoring_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :phone_monitoring, PhoneMonitoring.Mailer,
  adapter: Bamboo.TestAdapter

config :comeonin, :bcrypt_log_rounds, 4
